/*
  Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
*/

/// @author Nils Krumnack


//
// includes
//

#include <RootCoreUtils/Deprecated.h>

using namespace RCU;

//
// main program
//

RCU_DEPRECATED ("test")
void testDeprecated ();

int main ()
{
  return 0;
}
